﻿using System;

namespace Task12a
{
    class Program
    {
        static void Main(string[] args)
        {

            int[,] board = new int[8, 8];
            Console.WriteLine("Write what collumn and row you want to place your first queen");
            int collumn = Convert.ToInt16(Console.ReadLine());
            int row = Convert.ToInt16(Console.ReadLine());
            StartPlacing(board, row, collumn);

        }
        public static void StartPlacing(int[,] board, int row, int collumn)
        {
            //Skriv mer kode her for å plasere ut alle dronningen og start i posisjonen valgt
            board = Placequeen(row, collumn, board);
            //Place all 8 queens
            for (int t = 1; t < 8; t++)
            {
                if (GetSum(board) < 8)
                {
                    board = Placequeen(0, t, board);
                }
                else
                {
                    PrintBoard(board);
                    break;
                }
            }
        }
        public static int[,] Placequeen(int i, int j, int[,] board)
        {
            if (GetSum(board) == 8)
            {
                Console.WriteLine("You win");
                PrintBoard(board);
                return board;
            }
            if (CheckIfLegal(board, i, j) == true)
            {
                //Place queen
                board[i, j] = 1;
            }
            else
            {
                if (i + 1 < 8)
                {
                    Placequeen(i + 1, j, board);
                }
                else
                {

                    //Backtrack, Find the row of the last queen, remove the queen try with the queen i space lower.
                    TryAgain(board, i, j);
                }
            }
            return board;
        }
        public static bool CheckIfLegal(int[,] board, int i, int j)
        {
            for (int k = 0; k < 8; k++)
            {
                if (i > 7 || k > 7)
                {
                    return false;
                }
                //Checking rows and collumn for a queen
                if (board[i, k] == 1)
                {
                    return false;
                }
                if (board[k, j] == 1)
                {
                    return false;
                }
                //Checking every diagonal for a queen
                if ((i + k < 8 && j + k < 8) && board[i + k, j + k] == 1)
                {
                    return false;
                }
                if ((i + k < 8 && j - k >= 0) && board[i + k, j - k] == 1)
                {
                    return false;
                }
                if ((i - k >= 0 && j + k < 8) && board[i - k, j + k] == 1)
                {
                    return false;
                }
                if ((i - k >= 0 && j - k >= 0) && board[i - k, j - k] == 1)
                {
                    return false;
                }
            }
            return true;
        }
        public static int FindLastQueen(int[,] board, int c)
        {
            if (c==-1)
            {

            }
            for (int p = 0; p < 8; p++)
            {
                if (board[p, c] == 1)
                {
                    PrintBoard(board);
                    Console.WriteLine("Lastqueen is at {0},{1}", c,p);
                    board[p, c] = 0;
                    return p;
                }
            }
            // The queen was not found, try to search the board
            /*
            for (int i = 0; i < length; i++)
            {
                for (int i = 0; i < length; i++)
                {

                }
            }
            */
            Console.WriteLine("Why");
            return 99;
        }
        public static void TryAgain(int[,] board, int i, int j)
        {
            // Find the last row, and remove the queen
            int lr = FindLastQueen(board, j - 1);
            //Place a new queen if possible
            if (lr < 8)
            {
                if (GetSum(board) < 8)
                {
                    board = Placequeen(lr + 1, j - 1, board);
                }
            }
            //Start again in the row you were
            for (int t = j; t < 8; t++)
            {
                if (GetSum(board) < 8)
                {
                    board = Placequeen(0, t, board);
                }
            }
        }
        public static void PrintBoard(int[,] board)
        {
            for (int k = 0; k < 8; k++)
            {
                for (int l = 0; l < 8; l++)
                {
                    Console.Write(board[k, l]);
                }
                Console.WriteLine();
            }
        }
        public static int GetSum(int[,] board)
        {
            int s = 0;
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (board[i, j] == 1)
                    {
                        s = s + 1;
                    }
                }
            }
            return s;
        }
    }
}

